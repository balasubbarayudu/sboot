package com.example.sb.sboot.controller;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.sb.sboot.model.Employee;

@RestController
@CrossOrigin("*")
public class EmpController {
	
	@RequestMapping(value="/empdetails" , method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Employee getEmp()
	{
		Employee emp=new Employee();
		emp.setId(2);
		emp.setName("bala");
		return emp;
		
	}

	@RequestMapping(value="/postemp" , method=RequestMethod.POST , consumes=MediaType.APPLICATION_JSON_VALUE , produces=MediaType.APPLICATION_JSON_VALUE)
  public Employee postEmp( @RequestBody Employee employee)
  {
		
	  return employee;
  }
	

}
